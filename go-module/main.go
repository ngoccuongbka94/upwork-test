package main

import (
	"fmt"

	"gitlab.com/freelancer-cuong/upwork-test/hello"
)

func main() {
	var a, b = 5, 10
	fmt.Println("Sum of", a , "and" , b, "is", hello.Add(a, b))
}
